# Generated by Django 3.1.2 on 2020-12-02 01:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('noticias', '0012_auto_20201030_2134'),
    ]

    operations = [
        migrations.AlterField(
            model_name='noticias',
            name='imagen',
            field=models.ImageField(blank=True, null=True, upload_to='imagenes'),
        ),
    ]
